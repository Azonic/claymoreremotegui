#pragma once
/********************************************************************************
** Form generated from reading UI file 'ClayMoreConfigToolY26948.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef CLAYMORECONFIGTOOLY26948_H
#define CLAYMORECONFIGTOOLY26948_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_ReloadWarningPopup
{
public:
	QDialogButtonBox *buttonBox;
	QLabel *label;
	QLabel *label_2;

	void setupUi(QDialog *ReloadWarningPopup)
	{
		if (ReloadWarningPopup->objectName().isEmpty())
			ReloadWarningPopup->setObjectName(QStringLiteral("ReloadWarningPopup"));
		ReloadWarningPopup->resize(340, 144);
		buttonBox = new QDialogButtonBox(ReloadWarningPopup);
		buttonBox->setObjectName(QStringLiteral("buttonBox"));
		buttonBox->setGeometry(QRect(-40, 80, 341, 32));
		buttonBox->setOrientation(Qt::Horizontal);
		buttonBox->setStandardButtons(QDialogButtonBox::Cancel | QDialogButtonBox::Ok);
		label = new QLabel(ReloadWarningPopup);
		label->setObjectName(QStringLiteral("label"));
		label->setGeometry(QRect(50, 20, 101, 16));
		label_2 = new QLabel(ReloadWarningPopup);
		label_2->setObjectName(QStringLiteral("label_2"));
		label_2->setGeometry(QRect(50, 40, 61, 16));

		retranslateUi(ReloadWarningPopup);
		QObject::connect(buttonBox, SIGNAL(accepted()), ReloadWarningPopup, SLOT(accept()));
		QObject::connect(buttonBox, SIGNAL(rejected()), ReloadWarningPopup, SLOT(reject()));

		QMetaObject::connectSlotsByName(ReloadWarningPopup);
	} // setupUi

	void retranslateUi(QDialog *ReloadWarningPopup)
	{
		ReloadWarningPopup->setWindowTitle(QApplication::translate("ReloadWarningPopup", "Warning", Q_NULLPTR));
		label->setText(QApplication::translate("ReloadWarningPopup", "Changes will be lost!", Q_NULLPTR));
		label_2->setText(QApplication::translate("ReloadWarningPopup", "Continue?", Q_NULLPTR));
	} // retranslateUi

};

namespace Ui {
	class ReloadWarningPopup : public Ui_ReloadWarningPopup {};
} // namespace Ui

QT_END_NAMESPACE

#endif // CLAYMORECONFIGTOOLY26948_H
