#include "ClayMoreGui.h"

#include <iostream>

#include <NetworkManagerOSC.h>
#include "../cmUtil/ConfigReader.h"
#include "defines.h"
#include <osc/OscReceivedElements.h>
#include <osc/OscPacketListener.h>
#include <osc/OscOutboundPacketStream.h>
#include <ip/UdpSocket.h>

#include <thread>

ClayMoreGui::ClayMoreGui(QWidget *parent) : QMainWindow(parent)
{
	ui.setupUi(this);
	connectButtons();
	
	//NOTE: ConfigTool
	//m_ct.show();

	g_configReader = new ConfigReader();
	if (!g_configReader->readConfigFile(CONFIG_FILE_PATH))
	{
		system("pause");
		return;
	}

	//#### Page 2 ######
	ui.lineEdit_newValue->setEnabled(false);	//Edit Sektion ausgrauen
	ui.pushButton_edit->setEnabled(false);

	configFilePath = "data/config.xml";
	configFile.setFileName(configFilePath);
	readConfigFile();

	
}

ClayMoreGui::~ClayMoreGui()
{
	//NOTE: ConfigTool -> Everytime when closing the main app, the entire app crashes. For because of m_ct, but I have no clue how to stop it before.
	//m_ct.deleteLater();
}

void ClayMoreGui::buttonClickedConnect()
{
	std::cout << "buttonClickedConnect ...." << std::endl;
	m_networkManagerOSC = new NetworkManagerOSC(g_configReader);
	m_networkManagerOSC->activateStatusUpdateListener();
	std::thread thread0(&NetworkManagerOSC::run, m_networkManagerOSC);
	thread0.detach();
}

void ClayMoreGui::buttonClickedSendCommand()
{
	std::cout << "send message" << std::endl;
	m_networkManagerOSC->sendControlCommand("Pisse", "kotze");
}

//connect the inputs to the functions
void ClayMoreGui::connectButtons() 
{	
	//for debug
	std::cout << "Connecting buttons! ...." << std::endl;
	QObject::connect(ui.pushButton_connect, SIGNAL(clicked()), this, SLOT(buttonClickedConnect()));
	QObject::connect(ui.pushButton_send_command, SIGNAL(clicked()), this, SLOT(buttonClickedSendCommand()));

	//#### Page 2
	QObject::connect(ui.listWidget_xmlItems, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(selectXMLItem()));
	QObject::connect(ui.pushButton_edit, SIGNAL(clicked()), this, SLOT(buttonClickedEdit()));

}

//#### Page 2 #######

void ClayMoreGui::readConfigFile() 
{	
	if (configFile.open(QIODevice::ReadOnly)) {
		doc.setContent(&configFile);
		configFile.close();
	}
	else
	{
		configFile.close();
		std::cout << "Can't open ConfigFile" << std::endl;
		return;
	}

	QDomElement docElem = doc.documentElement();
	QDomNode configNode = docElem.firstChild();
	QDomNode startupConfigNode = configNode.firstChild();

	int i = 0;
	while (!startupConfigNode.isNull()) {
		QDomElement domElement = startupConfigNode.toElement(); // try to convert the node to an element.
		if (!domElement.isNull()) {
			//Der liste hinzuf�gen
			ui.listWidget_xmlItems->addItem(domElement.tagName());
			//std::cout << qPrintable(domElement.tagName()) << std::endl; // the node really is an element.
			ui.line_CurrentValue->text() = domElement.tagName();
		}
		i++;
		startupConfigNode = startupConfigNode.nextSibling();
	}	
}

void ClayMoreGui::selectXMLItem()
{
	//temporary variable to get the selected docElement
	QDomElement selectedElement;
	selectedElement = searchDocElement(ui.listWidget_xmlItems->currentItem()->text());

	//setting currentValue and selectedItem
	ui.line_CurrentValue->setText(selectedElement.firstChild().toText().substringData(0, 14));
	ui.line_SelectedItem->setText(selectedElement.tagName());

	//Enabling the edit line
	ui.pushButton_edit->setEnabled(true);
	ui.lineEdit_newValue->setEnabled(true);

	//for debug
	//std::cout << "Item selected?!" << std::endl;
}

//search for a DocElement in the xml file
QDomElement ClayMoreGui::searchDocElement(QString tagname)
{
	//-----------------same procedure as readFile()-----------------------------------
	//Following example code is from http://doc.qt.io/archives/qt-4.8/qdomdocument.html
	QDomElement docElem = doc.documentElement();
	QDomNode configNode = docElem.firstChild();
	QDomNode startupConfigNode = configNode.firstChild();
	QDomElement foundedDocElement;


	while (!startupConfigNode.isNull()) {
		QDomElement e = startupConfigNode.toElement();
		if (!e.isNull()) {
			//if the tagname match write into the return variable
			if (e.tagName() == tagname) {
				foundedDocElement = e;
			}
		}

		startupConfigNode = startupConfigNode.nextSibling();
	}

	//-------------------------------------------------------------------------------

	return foundedDocElement;
}

void ClayMoreGui::buttonClickedEdit()
{
	//Get element in question
	QString nameSelectedItem = ui.line_SelectedItem->text();
	QDomElement elementToReplace = searchDocElement(nameSelectedItem);
	QString newValue = ui.lineEdit_newValue->text();

	//std::cout << "Before:" <<  elementToReplace.text().toStdString() << std::endl;


	elementToReplace.firstChild().setNodeValue(newValue);

	//std::cout << "After:" << elementToReplace.text().toStdString() << std::endl;

	// Write changes to same file
	configFile.open(QIODevice::ReadWrite | QIODevice::Text);
	QTextStream stream;
	stream.setDevice(&configFile);
	doc.save(stream, QDomNode::EncodingFromDocument);

	configFile.close();

	std::cout << "e�frhgoaehrgoih" << std::endl;
	ui.line_CurrentValue->setText(searchDocElement(ui.line_SelectedItem->text()).firstChild().toText().substringData(0, 14));
	ui.lineEdit_newValue->clear();
	ui.pushButton_edit->setDisabled(true);
}