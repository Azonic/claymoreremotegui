#pragma once
#include <QtWidgets/QDialog>
#include <QtCore>
#include <QtGui>
#include <QtXML/QtXML>
#include <QtDebug>
#include "Ui_ConfirmationPopup.h"

class ConfirmationPopup : public QDialog
{
	Q_OBJECT

public:
	ConfirmationPopup(QWidget *parent = Q_NULLPTR);


private:
	Ui::ConfirmationPopup ui;
	QStandardItemModel *m_model;
};