#include "ReloadWarningPopup.h"
#include <iostream>
#include <QXmlStreamReader>
#include <QFile>
#include <QDir>
#include "ui_ReloadWarningPopup.h"

ReloadWarningPopup::ReloadWarningPopup(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	m_activated = false;
}

void ReloadWarningPopup::accept() {
	m_accept = true;
	done(1);

	//for debug
	std::cout << "YES" << std::endl;

}

void ReloadWarningPopup::reject() {
	m_accept = false;
	done(0);

	//for debug
	std::cout << "NO" << std::endl;

}

bool ReloadWarningPopup::getAccept() {
	return m_accept;
}

bool ReloadWarningPopup::getActivated() {
	return m_activated;
}
