#include "ClayMoreConfigTool.h"
#include <iostream>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QFile>
#include <QDir>
#include "ui_ClayMoreConfigTool.h"
#include "ReloadWarningPopup.h"
#include "ConfirmationPopup.h"

ClayMoreConfigTool::ClayMoreConfigTool(QWidget *parent) : QDialog(parent)
{
	//Setting up the UI
	ui.setupUi(this);
	ui.comboBox_language->addItem("english");
	ui.comboBox_language->addItem("german");
	ui.pushButton_edit->setEnabled(false);
	ui.lineEdit_newValue->setEnabled(false);
	connectButtons();

	//Setting up the files
	m_fileStandard.setFileName("configstandard.xml");
	m_filename = "data/config.xml";
	m_file.setFileName(m_filename);
	m_file.copy("m_fileTMP");
	m_fileTMP.setFileName("m_fileTMP");
	readFile();

	//Setting up the popups
	m_reloadWarningPopup = new ReloadWarningPopup();
	m_confirmationPopup = new ConfirmationPopup();
}

// changes the language of the application
void ClayMoreConfigTool::onLanguageChanged(int index)
{
	//for debug
	//std::cout << "Language Changed?!" << std::endl;
	//std::cout << index << std::endl;


	//english == 0 | german == 1 | ...
	//german
	switch (index)
	{
		case 0:			//english
			ui.label_CurrentValue->setText("Current Value");
			ui.label_language->setText("Language");
			ui.label_NewValue->setText("New Value");
			ui.label_path->setText("Path of string.xml File");
			ui.label_SelectedItem->setText("Selected Item");
			ui.pushButton_edit->setText("Edit Value");
			ui.pushButton_reload->setText("Discard Changes");
			ui.pushButton_restore->setText("Restore Defaults");
			ui.pushButton_write->setText("Store Changes");
			break;
		case 1:			//german
			ui.label_CurrentValue->setText("Aktueller Wert");
			ui.label_language->setText("Sprache");
			ui.label_NewValue->setText("Neuer Wert");
			ui.label_path->setText("Speicherort string.xml");
			ui.label_SelectedItem->setText("Ausgewaehlter Parameter");
			ui.pushButton_edit->setText("Wert bearbeiten");
			ui.pushButton_reload->setText("Veraenderungen verwerfen");
			ui.pushButton_restore->setText("Standard wiederherstellen");
			ui.pushButton_write->setText("Veraenderungen speichern");
			break;
		default:
			break;
	}
}

// Selecting a item from the list enabels the edit buttons and shows the current value
void ClayMoreConfigTool::selectXMLItem()
{
	//temporary variable to get the selected docElement
	QDomElement selectedElement;
	selectedElement = searchDocElement(ui.listWidget_xmlItems->currentItem()->text());

	//setting currentValue and selectedItem
	ui.line_CurrentValue->setText(selectedElement.firstChild().toText().substringData(0, 14));
	ui.line_SelectedItem->setText(selectedElement.tagName());
	
	//Enabling the edit line
	ui.lineEdit_newValue->setEnabled(true);

	//for debug
	//std::cout << "Item selected?!" << std::endl;
}

//handles the edit line to prevent saving nothing
void ClayMoreConfigTool::handleEditLine()
{
	//for debug
	std::cout << "Text change! ...." << std::endl;


	if (ui.lineEdit_newValue->text() == "") {
		//disable the edit button
		ui.pushButton_edit->setDisabled(true);
	}
	else {
		//enable the edit button
		ui.pushButton_edit->setEnabled(true);
	}
}

// Restores default options
void ClayMoreConfigTool::buttonClickedRestore()
{
	if (m_reloadWarningPopup->exec() == 1) {
		// Removing old files and overriding them with the standard file
		m_file.remove();
		m_fileStandard.copy(m_filename);
		m_file.setFileName(m_filename);

		m_fileTMP.remove();
		m_file.copy("m_fileTMP");
		m_fileTMP.setFileName("m_fileTMP");
	}
	else {
		//Not Reloading
		std::cout << "No Restore!" << std::endl;
	}


	//Reread the file
	readFile();
	ui.line_CurrentValue->setText(searchDocElement(ui.line_SelectedItem->text()).firstChild().toText().substringData(0, 14));
	//for debug
	//std::cout << "Restore?!" << std::endl;
}

// Overrides the actual file with the temporary one 
void ClayMoreConfigTool::buttonClickedWrite()
{
	//open the xml file
	m_file.remove();
	m_fileTMP.copy(m_filename);
	m_file.setFileName(m_filename);

	//Reread the file
	readFile();


	//popup 
	m_confirmationPopup->show();

	//for debug
	//std::cout << "Changes saved!" << std::endl;
}

// Overrides the value in the temporary file (TODO)
void ClayMoreConfigTool::buttonClickedEdit()
{
	//Get element in question
	QString nameSelectedItem = ui.line_SelectedItem->text();
	QDomElement elementToReplace = searchDocElement(nameSelectedItem);
	QString newValue = ui.lineEdit_newValue->text();

	//std::cout << "Before:" <<  elementToReplace.text().toStdString() << std::endl;


	elementToReplace.firstChild().setNodeValue(newValue);

	//std::cout << "After:" << elementToReplace.text().toStdString() << std::endl;

	// Write changes to same file
	m_fileTMP.open(QIODevice::ReadWrite | QIODevice::Text);
	QTextStream stream;
	stream.setDevice(&m_fileTMP);
	m_documentTMP.save(stream, QDomNode::EncodingFromDocument);

	m_fileTMP.close();

	ui.line_CurrentValue->setText(searchDocElement(ui.line_SelectedItem->text()).firstChild().toText().substringData(0, 14));
	ui.lineEdit_newValue->clear();
	ui.pushButton_edit->setDisabled(true);
}

// Reloads the file to start over
void ClayMoreConfigTool::buttonClickedReload()
{
	if (m_reloadWarningPopup->exec() == 1) {
		//Reloading....
		std::cout << "Reload!" << std::endl;

		m_fileTMP.remove();

		m_file.copy("m_fileTMP");
		m_fileTMP.setFileName("m_fileTMP");

		readFile();
		ui.line_CurrentValue->setText(searchDocElement(ui.line_SelectedItem->text()).firstChild().toText().substringData(0, 14));
	}
	else {
		//Not Reloading
		std::cout << "No Reload!" << std::endl;
	}

	//for debug
	//std::cout << "Reload?!" << std::endl;
}

// Reading the XML file and displaying it in the list
void ClayMoreConfigTool::readFile()
{
	//qDebug() << "File will be read now";

	ui.listWidget_xmlItems->clear();

	if (m_fileTMP.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		//qDebug() << "File was found and successfully opened";
		m_document.setContent(&m_fileTMP);
		m_documentTMP = m_document;
		m_fileTMP.close();
	}

	//Following example code is from http://doc.qt.io/archives/qt-4.8/qdomdocument.html
	QDomElement docElem = m_documentTMP.documentElement();
	QDomNode configNode = docElem.firstChild();
	QDomNode startupConfigNode = configNode.firstChild();

	int i = 0;

	while (!startupConfigNode.isNull()) {
		QDomElement domElement = startupConfigNode.toElement(); // try to convert the node to an element.
		if (!domElement.isNull()) {
			//Der liste hinzufügen
			ui.listWidget_xmlItems->addItem(domElement.tagName());
			std::cout << qPrintable(domElement.tagName()) << std::endl; // the node really is an element.
			ui.line_CurrentValue->text() = domElement.tagName();
		}
		i++;
		startupConfigNode = startupConfigNode.nextSibling();
	}

	if (!m_fileTMP.open(QIODevice::ReadOnly))
		return;
	if (!m_documentTMP.setContent(&m_fileTMP)) {
		m_fileTMP.close();
		return;
	}
	m_fileTMP.close();

}

//search for a DocElement in the xml file
QDomElement ClayMoreConfigTool::searchDocElement(QString tagname)
{
	//-----------------same procedure as readFile()-----------------------------------
	//Following example code is from http://doc.qt.io/archives/qt-4.8/qdomdocument.html
	QDomElement docElem = m_documentTMP.documentElement();
	QDomNode configNode = docElem.firstChild();
	QDomNode startupConfigNode = configNode.firstChild();
	QDomElement foundedDocElement;


	while (!startupConfigNode.isNull()) {
		QDomElement e = startupConfigNode.toElement();
		if (!e.isNull()) {
			//if the tagname match write into the return variable
			if (e.tagName() == tagname) {
				foundedDocElement = e;
			}
		}

		startupConfigNode = startupConfigNode.nextSibling();
	}

	//-------------------------------------------------------------------------------

	return foundedDocElement;
}

//connect the inputs to the functions
void ClayMoreConfigTool::connectButtons() {
	//for debug
	std::cout << "Connecting buttons! ...." << std::endl;

	QObject::connect(ui.pushButton_reload, SIGNAL(clicked()), this, SLOT(buttonClickedReload()));
	QObject::connect(ui.lineEdit_newValue, SIGNAL(textChanged(const QString &)), this, SLOT(handleEditLine()));
	QObject::connect(ui.pushButton_restore, SIGNAL(clicked()), this, SLOT(buttonClickedRestore()));
	QObject::connect(ui.pushButton_write, SIGNAL(clicked()), this, SLOT(buttonClickedWrite()));
	QObject::connect(ui.pushButton_edit, SIGNAL(clicked()), this, SLOT(buttonClickedEdit()));
	QObject::connect(ui.comboBox_language, SIGNAL(currentIndexChanged(int)), this, SLOT(onLanguageChanged(int)));
	QObject::connect(ui.listWidget_xmlItems, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(selectXMLItem()));

	//for debug
	std::cout << "Connecting buttons! Done!" << std::endl;

}
