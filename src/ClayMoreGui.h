#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_ClayMoreGui.h"

//NOTE: ConfigTool
#include "ClayMoreConfigTool.h";

//NOTE for Build:
//In CMakeLists.txt is a copy command for the Qt5 Binaries into the claymore/build/bin dir

class NetworkManagerOSC;
class ConfigReader;

class ClayMoreGui : public QMainWindow
{
	Q_OBJECT

public:
	ClayMoreGui(QWidget *parent = Q_NULLPTR);
	~ClayMoreGui();

private slots:
	//-------Button events----------//
	void buttonClickedConnect();
	void buttonClickedSendCommand();

	//#### Page 2
	void selectXMLItem();
	void buttonClickedEdit();

private:
	//NOTE: ConfigTool
	//ClayMoreConfigTool m_ct;

	Ui::ClayMoreGuiClass ui;
	NetworkManagerOSC* m_networkManagerOSC;
	ConfigReader* g_configReader;
	void connectButtons();	

	//#### Page 2 ################	

	QString configFilePath;
	QFile configFile;
	QDomDocument doc;

	void readConfigFile();
	QDomElement searchDocElement(QString tagname);

};
