#pragma once
#include <QtWidgets/QDialog>
#include <QtCore>
#include <QtGui>
#include <QtXML/QtXML>
#include <QtDebug>
#include "Ui_ReloadWarningPopup.h"

class ReloadWarningPopup : public QDialog
{
	Q_OBJECT

public:
	ReloadWarningPopup(QWidget *parent = Q_NULLPTR);
	bool getAccept();
	bool getActivated();
	


private:
	Ui::ReloadWarningPopup ui;
	QStandardItemModel *m_model;
	void accept();
	void reject();
	bool m_accept;
	bool m_activated;
};