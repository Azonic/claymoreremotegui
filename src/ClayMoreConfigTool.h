#pragma once

#include <QtWidgets/QDialog>
#include <QtCore>
#include <QtGui>
#include <QtXML/QtXML>
#include <QtDebug>
#include "ui_ClayMoreConfigTool.h"
#include "ReloadWarningPopup.h"
#include "ConfirmationPopup.h"


class ClayMoreConfigTool : public QDialog
{
	Q_OBJECT

public:
	ClayMoreConfigTool(QWidget *parent = Q_NULLPTR);

private slots:
	//-------Button events----------//
	void buttonClickedReload();
	void buttonClickedRestore();
	void buttonClickedWrite();	
	void buttonClickedEdit();
	
	//-------Input events----------//
	void onLanguageChanged(int index);
	void selectXMLItem();
	void handleEditLine();
	

private:
	Ui::ClayMoreConfigToolClass ui;
	QStandardItemModel *m_model;
	QString m_filename;
	QDomDocument m_document;
	QDomDocument m_documentTMP;
	ReloadWarningPopup *m_reloadWarningPopup;
	ConfirmationPopup *m_confirmationPopup;
	QFile m_file;
	QFile m_fileStandard;
	QFile m_fileTMP;
	
	void readFile();
	QDomElement searchDocElement(QString tagname);
	void connectButtons();
};
